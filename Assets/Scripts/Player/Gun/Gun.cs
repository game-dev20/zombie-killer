﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private GameObject _prefabBulletHole;
    [SerializeField] private GameObject _prefabBulletHoleBody;
    [SerializeField] private Transform _gunDirection;

    public GameObject objectParticleSystem;

    public int damageStrength = 10;
    public int currentNumberRounds = 30;
    public int numberRoundsMagazine = 30;
    public float rateFire = 0.5f;
    public float cooldownTime = 2.0f;

    private int _totalNumberElementsBulletHole = 10;
    private Dictionary<GameObject, System.Object> _dictionaryBulletHole = 
        new Dictionary<GameObject, System.Object>();
    private Dictionary<GameObject, int> _counterBulletHole = 
        new Dictionary<GameObject, int>();

    private void InitDictionaryBulletHole()
    {
        _counterBulletHole.Add(_prefabBulletHole, 0);
        _counterBulletHole.Add(_prefabBulletHoleBody, 0);

        _dictionaryBulletHole.Add(_prefabBulletHole, null);
        _dictionaryBulletHole.Add(_prefabBulletHoleBody, null);

        List<GameObject> keysDictionary = new List<GameObject>();
        foreach(GameObject prefab in _dictionaryBulletHole.Keys)
        {
            keysDictionary.Add(prefab);
        }

        foreach (GameObject prefab in keysDictionary)
        {
            List<GameObject> listObjectBulletHole = new List<GameObject>();

            for (int i = 0; i < _totalNumberElementsBulletHole; i++)
            {
                GameObject objectBulletHole = Instantiate(prefab, Vector3.zero, 
                    Quaternion.identity);
                objectBulletHole.SetActive(false);

                listObjectBulletHole.Add(objectBulletHole);
            }

            _dictionaryBulletHole[prefab] = listObjectBulletHole;
        }
    }

    private void Start()
    {
        InitDictionaryBulletHole();
    }

    public void Shoot()
    {
        Ray ray = new Ray(_gunDirection.position, _gunDirection.forward);
        RaycastHit hit;
        
        if (
            Physics.Raycast(ray, out hit, Mathf.Infinity, -1, 
            QueryTriggerInteraction.Ignore)
        )
        {
            Transform hitTransform = hit.transform;
            ReactiveTarget target = hitTransform.GetComponent<ReactiveTarget>();

            if (target != null)
            {
                Vector3 positionBodyEnemy = hitTransform.position;
                Vector3 pointBulllet = new Vector3(positionBodyEnemy.x, 
                    hit.point.y, positionBodyEnemy.z);
                CreateBulletHole(_prefabBulletHoleBody, pointBulllet, 
                    hit.normal, hitTransform);

                target.ReactToHit(damageStrength);
            }
            else
            {
                CreateBulletHole(_prefabBulletHole, hit.point, hit.normal);
            }
        }
    }

    private void CreateBulletHole(GameObject prefab, Vector3 pos, 
        Vector3 normal, Transform parent = null)
    {
        Quaternion projectorRotation = 
            Quaternion.FromToRotation(Vector3.forward, normal);

        int i = _counterBulletHole[prefab];
        List<GameObject> listObjectBulletHole  = 
            (List<GameObject>) _dictionaryBulletHole[prefab];
        GameObject objectBulletHole = listObjectBulletHole[i];
        Transform transformBulletHole = objectBulletHole.transform;
        transformBulletHole.position = pos;
        transformBulletHole.rotation = projectorRotation;
        transformBulletHole.parent = parent;
        objectBulletHole.SetActive(true);

        i++;
        if(i == _totalNumberElementsBulletHole)
        {
            i = 0;
        }
        _counterBulletHole[prefab] = i;

        StartCoroutine(DeactivationBulletHole(objectBulletHole));
    }

    IEnumerator DeactivationBulletHole(GameObject bulletHole)
    {
        yield return new WaitForSeconds(1f);

        bulletHole.SetActive(false);
        bulletHole.transform.parent = null;
    }
}
