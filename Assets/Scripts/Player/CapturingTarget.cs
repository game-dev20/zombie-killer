﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementPlayer))]
public class CapturingTarget : MonoBehaviour
{
    [SerializeField] private GameObject _selectionPictureEnemy;
    [SerializeField] private HealthLevel _sliderHealthEnemy;

    public float radiusDetection = 10.0f;
    public LayerMask layerSearch;
    public LayerMask layerGround;

    private Transform _transform;
    private Transform _transformPictureEnemy;
    private MovementPlayer _movementPlayer;

    private void Start()
    {
        _transformPictureEnemy = _selectionPictureEnemy.transform;
        _transformPictureEnemy.parent = null;
        _selectionPictureEnemy.SetActive(false);
        _transform = transform;
        _movementPlayer = GetComponent<MovementPlayer>();

        StartCoroutine(SearchGoals());
    }

    private IEnumerator SearchGoals()
    {
        while(true)
        {
            Vector3 transformPosition = _transform.position;
            Vector3 colliderFoundPosition;
            Collider[] collidersFound = Physics.OverlapSphere(
                transformPosition, radiusDetection, layerSearch);

            float minimumDistance = -1f;

            ReactiveTarget targetObject = null;
            _selectionPictureEnemy.SetActive(false);

            int health = 0;
            int maxHealth = 0;
            string keyName = "";

            foreach (Collider colliderFound in collidersFound)
            {
                colliderFoundPosition = colliderFound.transform.position;

                CharacteristicsEnemy enemy = 
                    colliderFound.GetComponent<CharacteristicsEnemy>();
                ReactiveTarget reactiveTarget = 
                    colliderFound.GetComponent<ReactiveTarget>();

                if (enemy == null || reactiveTarget == null)
                {
                    continue;
                }

                if (
                    !enemy.isAlive ||
                    (
                        Physics.Linecast(transformPosition, 
                        colliderFoundPosition, layerGround)
                    )
                )
                {
                    continue;
                }

                float itemDistance = 
                    Vector3.Distance(colliderFoundPosition, transformPosition);

                if (minimumDistance < 0 || itemDistance < minimumDistance)
                {
                    minimumDistance = itemDistance;

                    targetObject = reactiveTarget;
                    health = enemy.health;
                    maxHealth = enemy.maxHealth;
                    keyName = enemy.keyName;
                }
            }

            if(targetObject != null)
            {
                Transform transformTargetObject = targetObject.transform;
                Vector3 targetObjectPosition = transformTargetObject.position;
                targetObjectPosition.y = 0.1f;

                _transformPictureEnemy.parent = transformTargetObject;
                _transformPictureEnemy.position = targetObjectPosition;
                _transformPictureEnemy.rotation = Quaternion.identity;

                _selectionPictureEnemy.SetActive(true);

                _movementPlayer.targetObject = targetObject;

                _sliderHealthEnemy.SetTitle(keyName);
                _sliderHealthEnemy.SetHealth(health, maxHealth);
                _sliderHealthEnemy.SetEnabled(true);
            }
            else
            {
                _transformPictureEnemy.parent = null;
                _selectionPictureEnemy.SetActive(false);

                _movementPlayer.targetObject = null;

                _sliderHealthEnemy.SetEnabled(false);
            }

            yield return new WaitForSeconds(0.2f);
        }
    }

}
