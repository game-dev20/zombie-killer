﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacteristicsPlayer))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class MovementPlayer : MonoBehaviour
{
    [SerializeField] private VariableJoystick _joystick;
    [SerializeField] private BtnShoot _btnShoot;

    public ReactiveTarget targetObject = null;
    public float speedRotation = 15.0f;
    public float speedMove = 10.0f;

    private Animator _animator;
    private CharacterController _charController;
    private CharacteristicsPlayer _characteristicsPlayer;
    private Transform _transform;
    private Vector3 _targetPoint;

    void Start()
    {
        _transform = transform;
        _charController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _characteristicsPlayer = GetComponent<CharacteristicsPlayer>();
    }

    void Update()
    {
        if(!_characteristicsPlayer.isAlive)
        {
            return;
        }

        float inputHor = 0;
        float inputVert = 0;

        float inputHorKeyboard = Input.GetAxis("Horizontal");
        float inputVertKeyboard = Input.GetAxis("Vertical");

        float inputHorJoystick = _joystick.Horizontal;
        float inputVertJoystick = _joystick.Vertical;

        if(inputHorKeyboard != 0 || inputVertKeyboard != 0)
        {
            inputHor = inputHorKeyboard;
            inputVert = inputVertKeyboard;
        }
        else
        {
            inputHor = inputHorJoystick * 100;
            inputVert = inputVertJoystick * 100;
        }

        Vector3 targetObjectPosition = Vector3.zero;
        if (targetObject != null)
        {
            targetObjectPosition = targetObject.transformObject.position;
            targetObjectPosition.y = _transform.position.y;
        }

        Quaternion targetRot = Quaternion.identity;
        float currentSpeedRotation = speedRotation;

        if (targetObject != null && _btnShoot.isShoot)
        {
            _targetPoint = targetObjectPosition;
            currentSpeedRotation *= 2;
        }
        else if (inputHor != 0 || inputVert != 0)
        {
            _targetPoint = 
                new Vector3(inputHor, _transform.position.y, inputVert);
            _targetPoint += _transform.position;
        }
        else
        {
            _targetPoint = _transform.TransformPoint(Vector3.forward);
        }

        targetRot =
                Quaternion.LookRotation(_targetPoint - _transform.position);
        _transform.rotation = Quaternion.Slerp(_transform.rotation, targetRot,
            currentSpeedRotation * Time.deltaTime);

        Vector3 movement = Vector3.zero;
        if (inputHor != 0 || inputVert != 0)
        {
            movement = new Vector3(inputHor, 0f, inputVert);
            movement = Vector3.ClampMagnitude(movement, 1.0f);
        }

        Vector3 directionGaze = _targetPoint - _transform.position;
        directionGaze = Vector3.ClampMagnitude(directionGaze, 1.0f);
        Vector3 directionSide = 
            new Vector3(directionGaze.z, 0f, -directionGaze.x);

        float speedForward = Vector3.Dot(directionGaze, movement);
        float speedSide = Vector3.Dot(directionSide, movement);

        _animator.SetFloat("SpeedForward", speedForward);
        _animator.SetFloat("SpeedSide", speedSide);

        movement.y = -1.0f;
        movement *= speedMove * Time.deltaTime;

        _charController.Move(movement);
    }
}
