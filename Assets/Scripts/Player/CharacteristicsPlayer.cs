﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacteristicsPlayer : MonoBehaviour
{
    [SerializeField] private GameObject _bloodEffect;
    [SerializeField] private HealthLevel _sliderHealthPlayer;
    [SerializeField] private BtnRecharge _btnRecharge;

    public int maxHealth = 100;
    public int health = 100;
    public int maxCartridges = 1000;
    public int cartridges = 200;
    public bool isAlive = true;

    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _sliderHealthPlayer.SetHealth(health, maxHealth);
    }

    public void Hit(int value)
    {
        health -= value;
        if(health < 0)
        {
            health = 0;
        }

        _sliderHealthPlayer.SetHealth(health, maxHealth);

        if (health == 0)
        {
            isAlive = false;
            _animator.SetBool("IsDead", true);
        }
        else
        {
            StartCoroutine(BloodEffect());
        }
    }

    private IEnumerator BloodEffect()
    {
        _bloodEffect.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        _bloodEffect.SetActive(false);
    }

    public void SetHealth(int value)
    {
        health += value;
        if(health > maxHealth)
        {
            health = maxHealth;
        }
        _sliderHealthPlayer.SetHealth(health, maxHealth);
    }

    public void SetCartridges(int value)
    {
        cartridges += value;
        if(cartridges > maxCartridges)
        {
            cartridges = maxCartridges;
        }
        _btnRecharge.SetTextAmmoInventory(cartridges);
    }
}
