﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacteristicsPlayer))]
public class ShootingPlayer : MonoBehaviour
{
    [SerializeField] private BtnShoot _btnShoot;
    [SerializeField] private BtnRecharge _btnRecharge;
    [SerializeField] private Gun _gun;

    private CharacteristicsPlayer _characteristicsPlayer;
    private Animator _animator;
    private bool isShoot = false;
    private bool isRecharge = false;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _characteristicsPlayer = GetComponent<CharacteristicsPlayer>();

        _btnRecharge.SetTextAmmoMagazine(_gun.currentNumberRounds);
        _btnRecharge.SetTextAmmoInventory(_characteristicsPlayer.cartridges);
    }

    private void Update()
    {
        if(!_characteristicsPlayer.isAlive)
        {
            return;
        }

        if(
            _btnShoot.isShoot && 
            !isShoot && 
            !isRecharge && 
            (_gun.currentNumberRounds > 0)
        )
        {
            StartCoroutine(Shoot());
        }
        else if(
            (
                (
                    _btnShoot.isShoot &&
                    (_gun.currentNumberRounds == 0)
                ) || 
                _btnRecharge.isRecharge
            ) && 
            !isShoot && 
            !isRecharge &&
            (_characteristicsPlayer.cartridges > 0) &&
            (_gun.currentNumberRounds != _gun.numberRoundsMagazine)
        )
        {
            StartCoroutine(Recharge());
        }
    }

    private IEnumerator Shoot()
    {
        isShoot = true;
        float rateFire = _gun.rateFire;

        _animator.SetBool("IsShoot", true);
        _gun.objectParticleSystem.SetActive(true);

        while (_btnShoot.isShoot && _gun.currentNumberRounds > 0)
        {
            _gun.currentNumberRounds--;
            _gun.Shoot();

            _btnRecharge.SetTextAmmoMagazine(_gun.currentNumberRounds);

            yield return new WaitForSeconds(rateFire);
        }

        _animator.SetBool("IsShoot", false);
        _gun.objectParticleSystem.SetActive(false);
        isShoot = false;
    }

    private IEnumerator Recharge()
    {
        isRecharge = true;
        float cooldownTime = _gun.cooldownTime;

        _animator.SetBool("isRecharge", true);

        int numberRounds = 0;
        if(
            _characteristicsPlayer.cartridges >= _gun.numberRoundsMagazine - 
            _gun.currentNumberRounds
        )
        {
            numberRounds = _gun.numberRoundsMagazine - _gun.currentNumberRounds;
        }
        else
        {
            numberRounds = _characteristicsPlayer.cartridges;
        }
        _gun.currentNumberRounds += numberRounds;
        _characteristicsPlayer.cartridges -= numberRounds;

        yield return new WaitForSeconds(cooldownTime);

        _btnRecharge.SetTextAmmoMagazine(_gun.currentNumberRounds);
        _btnRecharge.SetTextAmmoInventory(_characteristicsPlayer.cartridges);

        _animator.SetBool("isRecharge", false);
        isRecharge = false;
    }
}
