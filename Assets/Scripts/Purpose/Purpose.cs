﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxCollider))]
public class Purpose : MonoBehaviour
{
    public PurposeType purposeType;

    static public Purpose purposeCompleteLevel;

    private Animator _animator;
    private BoxCollider _collider;
    private GameObject _gameObject;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<BoxCollider>();
        _gameObject = gameObject;
        if (purposeType == PurposeType.CompleteLevel)
        {
            purposeCompleteLevel = this;
            _gameObject.SetActive(false);
        }
    }

    public void ActivatingGoal()
    {
        _gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_gameObject == null)
        {
            return;
        }
        else if (!_gameObject.activeSelf)
        {
            return;
        }

        CharacteristicsPlayer player =
            other.GetComponent<CharacteristicsPlayer>();

        if (player == null)
        {
            return;
        }

        ControllerTasks controllerTasks = ControllerTasks.instance;
        if (controllerTasks != null)
        {
            if (purposeType == PurposeType.MainPurpose)
            {
                controllerTasks.MainTasksCompleted++;
            }
            else if (purposeType == PurposeType.AdditionalPurpose)
            {
                controllerTasks.AdditionalTasksCompleted++;
            }
            else
            {
                controllerTasks.CompleteLevel();
            }
        }

        _animator.SetBool("IsHide", true);
        _collider.enabled = false;
    }
}
