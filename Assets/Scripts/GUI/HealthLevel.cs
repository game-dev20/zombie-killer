﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class HealthLevel: MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private Text _title;

    public void SetHealth(int value, int maxValue)
    {
        _slider.maxValue = maxValue;
        _slider.value = value;
    }

    public void SetTitle(string value)
    {
        string text = LocalizationManager.Localize(value);

        _title.text = text;
    }

    public void SetEnabled(bool value)
    {
        gameObject.SetActive(value);
    }
}
