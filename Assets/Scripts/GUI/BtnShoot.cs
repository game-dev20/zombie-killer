﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnShoot : MonoBehaviour
{
    [SerializeField] private RectTransform _rectCircleBox;
    [SerializeField] private RectTransform _rectCenter;

    public bool isShoot = false;

    public void OnBtnDown()
    {
        _rectCircleBox.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        _rectCenter.localScale = new Vector3(1.2f, 1.2f, 1.2f);

        isShoot = true;
    }

    public void OnBtnUp()
    {
        _rectCircleBox.localScale = new Vector3(1f, 1f, 1f);
        _rectCenter.localScale = new Vector3(1f, 1f, 1f);

        isShoot = false;
    }
}
