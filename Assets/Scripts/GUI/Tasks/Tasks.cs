﻿using System.Collections;
using UnityEngine;

public class Tasks : MonoBehaviour
{
    [SerializeField] private GameObject _windowTasks;

    public void OnOpenTasks()
    {
        Time.timeScale = 0;
        _windowTasks.SetActive(true);
    }
}