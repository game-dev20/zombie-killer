﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WindowTasks : MonoBehaviour
{
    public Text task1;
    public Text task2;
    public Text task3;
    public Text task4;

    public Color colorTasksDefault;
    public Color colorTasksCompleted;

    public void OnCloseWindowTasks()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }
}
