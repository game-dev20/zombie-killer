﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnRecharge : MonoBehaviour
{
    [SerializeField] private RectTransform _rectBlockImage;
    [SerializeField] private Text _textAmmoMagazine;
    [SerializeField] private Text _textAmmoInventory;

    public bool isRecharge = false;

    public void OnBtnDown()
    {
        _rectBlockImage.localScale = new Vector3(0.9f, 0.9f, 0.9f);

        isRecharge = true;
    }

    public void OnBtnUp()
    {
        _rectBlockImage.localScale = new Vector3(1f, 1f, 1f);

        isRecharge = false;
    }

    public void SetTextAmmoMagazine(int value)
    {
        _textAmmoMagazine.text = value.ToString();
    }

    public void SetTextAmmoInventory(int value)
    {
        _textAmmoInventory.text = value.ToString();
    }
}
