﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCamera : MonoBehaviour
{
    [SerializeField] private Transform target;

    private float _rotY;
    private Vector3 _offset;
    private Quaternion _rotation;
    private Transform _transform;

    // Start is called before the first frame update
    void Start()
    {
        _transform = transform;
        _rotY = _transform.eulerAngles.y;
        _rotation = Quaternion.Euler(0, _rotY, 0);
        _offset = _rotation * (target.position - _transform.position);
    }

    void LateUpdate()
    {
        _transform.position = target.position - _offset;
        _transform.LookAt(target);
    }
}
