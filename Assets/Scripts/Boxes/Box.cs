﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Animator))]
public class Box : MonoBehaviour
{
    public int health = 100;
    public int cartridges = 100;

    public bool isOpen = false;

    public int indexType;
    public GameObject gameObjectBox;
    public Transform transformBox;
    public Animator animatorBox;
    
    public void Initialize()
    {
        transformBox = transform;
        gameObjectBox = gameObject;
        animatorBox = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        CharacteristicsPlayer player = 
            other.GetComponent<CharacteristicsPlayer>();
        
        if (player != null && !isOpen)
        {
            int count = health + cartridges;
            if(player.health == player.maxHealth)
            {
                count -= health;
            }
            if(player.cartridges == player.maxCartridges)
            {
                count -= cartridges;
            }
            if(count == 0)
            {
                return;
            }

            isOpen = true;

            player.SetHealth(health);
            player.SetCartridges(cartridges);

            animatorBox.SetBool("IsOpen", true);
            StartCoroutine(RemoveBox());
        }
    }

    private IEnumerator RemoveBox()
    {
        yield return new WaitForSeconds(1f);
        gameObjectBox.SetActive(false);
    }
}
