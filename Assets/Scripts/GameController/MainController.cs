﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(MapCellGenerator))]
[RequireComponent(typeof(ControllerBoxes))]
[RequireComponent(typeof(ControllerEnemies))]
[RequireComponent(typeof(ControllerTasks))]
public class MainController : MonoBehaviour
{
    [SerializeField] private GameObject _player;

    static public MapCellGenerator mapCellGenerator;
    static public ControllerBoxes boxes;
    static public ControllerEnemies enemies;
    static public ControllerTasks tasks;

    private void Awake()
    {
        mapCellGenerator = GetComponent<MapCellGenerator>();
        boxes = GetComponent<ControllerBoxes>();
        enemies = GetComponent<ControllerEnemies>();
        tasks = GetComponent<ControllerTasks>();

        mapCellGenerator.GenerateMap();
        boxes.Init();
        enemies.Init();
        tasks.Init();

        _player.SetActive(true);
    }
}
