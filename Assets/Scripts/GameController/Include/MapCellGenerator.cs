﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCellGenerator : MonoBehaviour
{
    [SerializeField] private Cell _prefabCell;
    [SerializeField] private Transform _objectParent;
    [SerializeField] private GameObject _prefabMainGoal;
    [SerializeField] private GameObject _prefabAdditionalGoal;
    [SerializeField] private GameObject _prefabGoalCompletedLevel;
    [SerializeField] private GameObject[] _prefabDeadCharacters;

    private List<MazeGeneratoeCell> _activeCellsLeadingTarget = 
        new List<MazeGeneratoeCell>();
    private PurposeType _whatGoalLeading = PurposeType.MainPurpose;

    private int _seed;
    private int _numberHalls;
    private int _numberSmallRooms;
    private int _numberAdditionalDoors;
    private int _numberCorpses;
    private int _numberDecorations;
    private int _widthMap;
    private int _heightMap;
    private float _cellWidth;
    private float _cellHeight;
    private float _offsetWidth;
    private float _offsetHeight;
    
    public void GenerateMap()
    {
        _seed = Manager.loadScene.seed;
        _numberHalls = Manager.loadScene.numberHalls;
        _numberSmallRooms = Manager.loadScene.numberSmallRooms;
        _numberAdditionalDoors = Manager.loadScene.numberAdditionalDoors;
        _numberCorpses = Manager.loadScene.numberCorpses;
        _numberDecorations = Manager.loadScene.numberDecorations;
        _widthMap = Manager.loadScene.widthMap;
        _heightMap = Manager.loadScene.heightMap;
        _cellWidth = Manager.loadScene.cellWidth;
        _cellHeight = Manager.loadScene.cellHeight;
        _offsetWidth = _cellWidth / 2f;
        _offsetHeight = _cellHeight / 2f;

        MazeGenerator generator = new MazeGenerator(_widthMap, _heightMap, 
            _numberHalls, _numberSmallRooms, _numberAdditionalDoors, 
            _numberCorpses, _numberDecorations, _seed);
        MazeGeneratoeCell[,] maze = generator.GenerateMaze();

        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        int countDeadCharacters = _prefabDeadCharacters.Length;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector3 position = 
                    new Vector3(x * _cellWidth, 0f, y * _cellHeight);
                Cell cell = Instantiate(_prefabCell, position, 
                    Quaternion.identity, _objectParent);

                MazeGeneratoeCell mazeCell = maze[x, y];

                if (x == width - 1)
                {
                    mazeCell.isWallRight = true;
                }
                if (y == height - 1)
                {
                    mazeCell.isWallTop = true;
                }
                if (x == 0)
                {
                    cell.RemoveExternalLightLeft();
                }
                if (y == 0)
                {
                    cell.RemoveExternalLightBottom();
                }

                bool isWallLeft = mazeCell.isWallLeft;
                bool isWallBottom = mazeCell.isWallBottom;
                bool isWallRight = mazeCell.isWallRight;
                bool isWallTop = mazeCell.isWallTop;
                bool isDoorLeft = mazeCell.isDoorLeft;
                bool isDoorBottom = mazeCell.isDoorBottom;
                bool isLight = mazeCell.isLight;

                if (!isWallLeft)
                {
                    cell.RemoveWallLeft();
                }
                if (!isWallBottom)
                {
                    cell.RemoveWallBottom();
                }
                if (!isWallRight)
                {
                    cell.RemoveWallRight();
                }
                if (!isWallTop)
                {
                    cell.RemoveWallTop();
                }
                if (!isDoorLeft)
                {
                    cell.RemoveDoorLeft();
                }
                if (!isDoorBottom)
                {
                    cell.RemoveDoorBottom();
                }

                if (!isLight)
                {
                    cell.RemoveLight();
                }
                else
                {
                    cell.RemoveNonGlowingLamp();
                }

                if (x == 0 && y == 0)
                {
                    cell.RemoveTriggerDoorBottom();
                }

                Vector3 positionTarget = position + 
                    new Vector3(_offsetWidth, 0f, _offsetHeight);
                if (mazeCell.isMainGoal)
                {
                    Instantiate(_prefabMainGoal, positionTarget, 
                        Quaternion.identity);
                }
                else if (mazeCell.isAdditionalGoal)
                {
                    Instantiate(_prefabAdditionalGoal, positionTarget, 
                        Quaternion.identity);
                }
                else if (mazeCell.isGoalCompletedLevel)
                {
                    Instantiate(_prefabGoalCompletedLevel, positionTarget, 
                        Quaternion.identity);
                }
                else if (mazeCell.isCorpse && countDeadCharacters > 0)
                {
                    int index = UnityEngine.Random.Range(0, countDeadCharacters);
                    GameObject prefabCorpse = _prefabDeadCharacters[index];
                    Instantiate(prefabCorpse, positionTarget, 
                        Quaternion.identity);
                }

                if (mazeCell.isBloodFloor)
                {
                    cell.InstallationBloodFloor();
                }
                if (mazeCell.isBloodWallBottomInside)
                {
                    cell.InstallationBloodWallBottomInside();
                }
                if (mazeCell.isBloodWallBottomOutside)
                {
                    cell.InstallationBloodWallBottomOutside();
                }
                if (mazeCell.isBloodWallLeftInside)
                {
                    cell.InstallationBloodWallLeftInside();
                }
                if (mazeCell.isBloodWallLeftOutside)
                {
                    cell.InstallationBloodWallLeftOutside();
                }

                if (mazeCell.isScenery)
                {
                    cell.InstallationDecorations();
                }

                cell.text.text = mazeCell.distanceStart.ToString();
            }
        }

        GC.Collect();
    }
}
