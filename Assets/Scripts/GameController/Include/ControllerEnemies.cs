﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerEnemies : MonoBehaviour
{
    [SerializeField] private CharacteristicsEnemy[] _prefabEnemies;
    [SerializeField] private CharacterController _player;
    [SerializeField] private HealthLevel _sliderHealthEnemy;

    public LayerMask layerEnemy;
    public LayerMask layerGround;

    public int maximumNumberEnemiesScene = 5;
    public float minRadiusEnemyAppearance = 15.0f;
    public float maxRadiusEnemyAppearance = 25.0f;
    public float maximumВistanceEnemy = 30.0f;
    public float distanceObstacles = 5.0f;
    public float radiusDetection = 10.0f;
    public float radiusReachingPoint = 1.0f;
    public float timeAnxiety = 1.0f;

    private int _currentNumberEnemiesScene = 0;
    private bool _isInitialized = false;

    private float _objectCreationBoundariesMinX;
    private float _objectCreationBoundariesMaxX;
    private float _objectCreationBoundariesMinZ;
    private float _objectCreationBoundariesMaxZ;
    private float _cellWidth;
    private float _cellHeight;
    private float _offsetX;
    private float _offsetZ;
    private int _widthMap;
    private int _heightMap;
    private Dictionary<string, int> _remainingNumberEnemies;
    private List<CharacteristicsEnemy> _enemies = 
        new List<CharacteristicsEnemy>();
    private CharacteristicsPlayer _characteristicsPlayer;
    private Transform _transformPlayer;

    public void Init()
    {
        _widthMap = Manager.loadScene.widthMap;
        _heightMap = Manager.loadScene.heightMap;
        _cellWidth = Manager.loadScene.cellWidth;
        _cellHeight = Manager.loadScene.cellHeight;
        _offsetX = _cellWidth / 2f;
        _offsetZ = _cellHeight / 2f;

        _objectCreationBoundariesMinX = _offsetX;
        _objectCreationBoundariesMaxX = _offsetX + (_widthMap - 1) * _cellWidth;
        _objectCreationBoundariesMinZ = _offsetZ;
        _objectCreationBoundariesMaxZ = _offsetZ + (_heightMap - 1) * 
            _cellHeight;

        _remainingNumberEnemies = 
            new Dictionary<string, int>(Manager.loadScene.maximumNumberEnemies);

        _characteristicsPlayer = _player.GetComponent<CharacteristicsPlayer>();
        _transformPlayer = _player.transform;

        StartCoroutine(RemovingDistantEnemy());
        StartCoroutine(CreateObjectEnemies());
        StartCoroutine(ChangeEnemyMode());
        StartCoroutine(DetectionTargets());
        StartCoroutine(DetectionObstacles());

        _isInitialized = true;
    }

    private IEnumerator RemovingDistantEnemy()
    {
        while (true)
        {
            Vector3 positionPlayer = _transformPlayer.position;
            foreach (CharacteristicsEnemy itemEnemy in _enemies)
            {
                if (!itemEnemy.isAlive)
                {
                    continue;
                }

                Vector3 PositionEnemy = itemEnemy.transformEnemy.position;
                if (
                    Vector3.Distance(positionPlayer, PositionEnemy) >
                    maximumВistanceEnemy
                )
                {
                    string keyName = itemEnemy.keyName;
                    itemEnemy.isAlive = false;
                    itemEnemy.isDecayed = true;
                    itemEnemy.waitingDecay = true;
                    itemEnemy.gameObjectEnemy.SetActive(false);
                    _currentNumberEnemiesScene--;
                    _remainingNumberEnemies[keyName]++;
                }
            }

            yield return new WaitForSeconds(5f);
        }
    }

    private IEnumerator CreateObjectEnemies()
    {
        while (true)
        {
            int numberMissilesCreated = maximumNumberEnemiesScene - 
                _currentNumberEnemiesScene;
            for (int i = 1; i <= numberMissilesCreated; i++)
            {
                List<string> listEnemyTypes = new List<string>();
                int currentNumberEnemyTypes = 0;
                foreach (var itemNumberEnemies in _remainingNumberEnemies)
                {
                    int count = itemNumberEnemies.Value;
                    string keyNameType = itemNumberEnemies.Key;
                    if (count > 0)
                    {
                        currentNumberEnemyTypes++;
                        listEnemyTypes.Add(keyNameType);
                    }
                }

                if (currentNumberEnemyTypes == 0)
                {
                    break;
                }

                int indexType = Random.Range(0, currentNumberEnemyTypes);
                string keyName = listEnemyTypes[indexType];

                bool isEnemyCreated = false;
                foreach (CharacteristicsEnemy itemEnemy in _enemies)
                {
                    if (itemEnemy.keyName == keyName && itemEnemy.isDecayed)
                    {
                        isEnemyCreated = true;

                        itemEnemy.waitingDecay = false;
                        itemEnemy.isDecayed = false;
                        itemEnemy.isAlive = true;
                        itemEnemy.enemyMode = EnemyMode.PeaceMind;
                        itemEnemy.health = itemEnemy.maxHealth;

                        Vector3 positionCreate = GetRandomPosition();
                        itemEnemy.transformEnemy.position = positionCreate;
                        itemEnemy.animatorEnemy.SetBool("IsWalk", false);
                        itemEnemy.animatorEnemy.SetBool("IsRun", false);
                        itemEnemy.animatorEnemy.SetBool("IsAttack", false);
                        itemEnemy.animatorEnemy.SetBool("IsDead", false);
                        itemEnemy.characterControllerEnemy.enabled = true;
                        itemEnemy.gameObjectEnemy.SetActive(true);

                        break;
                    }
                }
                if (!isEnemyCreated)
                {
                    CharacteristicsEnemy prefabEnemy = null;
                    foreach (CharacteristicsEnemy itemPrefab in _prefabEnemies)
                    {
                        if (itemPrefab.keyName == keyName)
                        {
                            prefabEnemy = itemPrefab;
                            break;
                        }
                    }

                    if (prefabEnemy == null)
                    {
                        Debug.LogError( "When creating an enemy, the enemy's " +
                            "prefab was not assigned");
                        StopCoroutine(CreateObjectEnemies());
                        yield break;
                    }

                    Vector3 positionCreate = GetRandomPosition();
                    CharacteristicsEnemy newEnemy = Instantiate(prefabEnemy, 
                        positionCreate, Quaternion.identity);
                    newEnemy.Initialize();
                    newEnemy.reactiveTargetEnemy.sliderHealthEnemy =
                        _sliderHealthEnemy;

                    _enemies.Add(newEnemy);
                }
                _remainingNumberEnemies[keyName]--;
                _currentNumberEnemiesScene++;
            }
            
            yield return new WaitForSeconds(5f);
        }
    }

    private Vector3 GetRandomPosition()
    {
        Vector3 positionPlayer = _transformPlayer.position;
        Vector3 position = Vector3.zero;
        float minDistanceEnemies;
        float minDistancePlayer;
        do
        {
            float angle = Random.Range(0f, 360f) * Mathf.PI / 180f;
            float radiusCreate = Random.Range(minRadiusEnemyAppearance,
                maxRadiusEnemyAppearance);
            float posX = positionPlayer.x + radiusCreate * Mathf.Cos(angle);
            float posZ = positionPlayer.z + radiusCreate * Mathf.Sin(angle);
            posX = Mathf.Round(posX / _cellWidth) * _cellWidth + _offsetX;
            posZ = Mathf.Round(posZ / _cellHeight) * _cellHeight + _offsetZ;
            position = new Vector3(posX, 0f, posZ);

            if (position.x < _objectCreationBoundariesMinX)
            {
                position.x = _objectCreationBoundariesMinX;
            }
            else if (position.x > _objectCreationBoundariesMaxX)
            {
                position.x = _objectCreationBoundariesMaxX;
            }
            if (position.z < _objectCreationBoundariesMinZ)
            {
                position.z = _objectCreationBoundariesMinZ;
            }
            else if (position.z > _objectCreationBoundariesMaxZ)
            {
                position.z = _objectCreationBoundariesMaxZ;
            }

            minDistancePlayer = Vector3.Distance(position, positionPlayer);
            minDistanceEnemies = 2f;
            foreach(CharacteristicsEnemy itemEnemy in _enemies)
            {
                Vector3 positionEnemy = itemEnemy.transformEnemy.position;
                float itemDistance = Vector3.Distance(position, positionEnemy);
                if (itemDistance < minDistanceEnemies)
                {
                    minDistanceEnemies = itemDistance;
                }
            }
        }
        while (
            (minDistancePlayer < minRadiusEnemyAppearance) || 
            (minDistanceEnemies < 1f)
        );
        
        return position;
    }

    private IEnumerator ChangeEnemyMode()
    {
        while (true)
        {
            int i = 0;
            foreach (CharacteristicsEnemy itemEnemy in _enemies)
            {
                if (
                    itemEnemy.isAlive && 
                    (itemEnemy.enemyMode != EnemyMode.Attack)
                )
                {
                    if (Random.Range(0, 3) == 0)
                    {
                        itemEnemy.enemyMode = EnemyMode.PeaceMind;

                        itemEnemy.animatorEnemy.SetBool("IsWalk", false);
                        itemEnemy.animatorEnemy.SetBool("IsRun", false);
                        itemEnemy.animatorEnemy.SetBool("IsAttack", false);
                    }
                    else
                    {
                        itemEnemy.enemyMode = EnemyMode.Patrolling;

                        itemEnemy.animatorEnemy.SetBool("IsWalk", true);
                        itemEnemy.animatorEnemy.SetBool("IsRun", false);
                        itemEnemy.animatorEnemy.SetBool("IsAttack", false);
                    }

                    if (Random.Range(0, 2) == 0)
                    {
                        float angleRotationChange =
                            Random.Range(-180.0f, 180.0f);
                        Vector3 vectorRotationChange =
                            new Vector3(0f, angleRotationChange, 0f);

                        itemEnemy.angleRotation = 
                            itemEnemy.transformEnemy.rotation * 
                            Quaternion.Euler(vectorRotationChange);
                    }
                    else
                    {
                        itemEnemy.angleRotation = 
                            itemEnemy.transformEnemy.rotation;
                    }
                }
                else if (!itemEnemy.isAlive && !itemEnemy.waitingDecay)
                {
                    StartCoroutine(ProcessDecay(itemEnemy));
                }

                i++;
            }

            yield return new WaitForSeconds(1f);
        }
    }

    private IEnumerator ProcessDecay(CharacteristicsEnemy enemy)
    {
        enemy.waitingDecay = true;
        yield return new WaitForSeconds(5f);

        enemy.processDecay = true;
        yield return new WaitForSeconds(2f);
        enemy.processDecay = false;
        enemy.gameObjectEnemy.SetActive(false);
        enemy.isDecayed = true;

        _currentNumberEnemiesScene--;
    }

    private IEnumerator DetectionTargets()
    {
        while (true)
        {
            Vector3 positionPlayer = _transformPlayer.position;
            Collider[] collidersFound = Physics.OverlapSphere(positionPlayer,
                radiusDetection, layerEnemy);
            foreach (Collider colliderFound in collidersFound)
            {
                float timeNow = Time.realtimeSinceStartup;
                Vector3 positionTarget = colliderFound.transform.position;
                Vector3 directionPlayer = positionPlayer - positionTarget;
                directionPlayer = directionPlayer.normalized;
                Vector3 directionEnemy = colliderFound.transform.forward;

                CharacteristicsEnemy itemEnemy =
                    colliderFound.GetComponent<CharacteristicsEnemy>();

                if (itemEnemy == null)
                {
                    continue;
                }

                if (!itemEnemy.isAlive)
                {
                    continue;
                }

                if (
                    (Vector3.Dot(directionEnemy, directionPlayer) < 0.2f) &&
                    (itemEnemy.enemyMode != EnemyMode.Attack)
                )
                {
                    continue;
                }

                if (Physics.Linecast(positionTarget, positionPlayer, layerGround))
                {
                    continue;
                }

                itemEnemy.lastPositionGoal = positionPlayer;
                itemEnemy.enemyMode = EnemyMode.Attack;
                itemEnemy.recentExcitement = timeNow;
                itemEnemy.animatorEnemy.SetBool("IsWalk", false);
                itemEnemy.animatorEnemy.SetBool("IsRun", true);
                itemEnemy.animatorEnemy.SetBool("IsAttack", false);
            }

            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator DetectionObstacles()
    {
        while (true)
        {
            foreach (CharacteristicsEnemy itemEnemy in _enemies)
            {
                if (
                    itemEnemy.isAlive && itemEnemy.enemyMode != EnemyMode.Attack
                )
                {
                    Vector3 positionEnemy = itemEnemy.transformEnemy.position;
                    Vector3 pointAreaChecked = 
                        itemEnemy.transformEnemy.position + 
                        itemEnemy.transformEnemy.forward * distanceObstacles;
                    if (Physics.Linecast(positionEnemy, pointAreaChecked))
                    {
                        float angleRotationChange =
                            Random.Range(100.0f, 180.0f);
                        if (Random.Range(0, 2) == 0)
                        {
                            angleRotationChange *= -1;
                        }
                        Vector3 vectorRotationChange =
                            new Vector3(0f, angleRotationChange, 0f);

                        itemEnemy.angleRotation = 
                            itemEnemy.transformEnemy.rotation * 
                            Quaternion.Euler(vectorRotationChange);
                    }
                }
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator BanStrikes(CharacteristicsEnemy enemy)
    {
        enemy.notHit = true;
        yield return new WaitForSeconds(enemy.timeBetweenStrokes);
        enemy.notHit = false;
    }

    private void Update()
    {
        if (!_isInitialized)
        {
            return;
        }

        float timeNow = Time.realtimeSinceStartup;
        foreach (CharacteristicsEnemy itemEnemy in _enemies)
        {
            float speed = 0f;
            float speedRotation = itemEnemy.speedRotation;
            Quaternion angleRotation = itemEnemy.angleRotation;
            Vector3 movement = 
                itemEnemy.transformEnemy.TransformVector(Vector3.forward);
            
            if (itemEnemy.enemyMode == EnemyMode.Patrolling)
            {
                speed = itemEnemy.walkingSpeed;
            }
            else if (itemEnemy.enemyMode == EnemyMode.Attack)
            {
                speed = itemEnemy.runningSpeed;
                speedRotation *= 10;

                Vector3 currentPositionGoal = _transformPlayer.position;
                Vector3 lastPositionGoal = itemEnemy.lastPositionGoal;
                Vector3 positionEnemy = itemEnemy.transformEnemy.position;
                Vector3 directionEnemy = lastPositionGoal - positionEnemy;
                angleRotation = Quaternion.LookRotation(directionEnemy);
                float distanceLastPositionGoal = 
                    Vector3.Distance(positionEnemy, lastPositionGoal);
                float distanceCurrentPositionGoal = 
                    Vector3.Distance(positionEnemy, currentPositionGoal);

                if (
                    distanceCurrentPositionGoal <= 
                    itemEnemy.distanceStrike + (speed * Time.deltaTime)
                )
                {
                    speed = 0f;
                    itemEnemy.animatorEnemy.SetBool("IsWalk", false);
                    itemEnemy.animatorEnemy.SetBool("IsRun", false);
                    itemEnemy.animatorEnemy.SetBool("IsAttack", true);

                    if(!itemEnemy.notHit && itemEnemy.isAlive)
                    {
                        _characteristicsPlayer.Hit(itemEnemy.forceBlow);
                        StartCoroutine(BanStrikes(itemEnemy));
                    }
                }
                else if (
                    (
                        distanceLastPositionGoal <= 
                        radiusReachingPoint + (speed * Time.deltaTime)
                    ) || 
                    (
                        (timeNow - itemEnemy.recentExcitement > timeAnxiety) && 
                        (itemEnemy.recentExcitement > 0f)
                    )
                )
                {
                    itemEnemy.enemyMode = EnemyMode.PeaceMind;
                    speed = 0f;
                    itemEnemy.animatorEnemy.SetBool("IsWalk", false);
                    itemEnemy.animatorEnemy.SetBool("IsRun", false);
                    itemEnemy.animatorEnemy.SetBool("IsAttack", false);
                }
            }
            
            if(itemEnemy.isAlive)
            {
                itemEnemy.transformEnemy.rotation = Quaternion.Slerp(
                    itemEnemy.transformEnemy.rotation, angleRotation, 
                    speedRotation * Time.deltaTime);

                movement.y = -1.0f;
                movement *= speed * Time.deltaTime;

                itemEnemy.characterControllerEnemy.Move(movement);
            }
            else if(itemEnemy.processDecay)
            {
                itemEnemy.transformEnemy
                    .Translate(Vector3.down * Time.deltaTime);
            }
        }
    }
}
