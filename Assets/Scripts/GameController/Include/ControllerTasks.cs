﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SimpleLocalization;

public class ControllerTasks : MonoBehaviour
{
    [SerializeField] private WindowTasks _windowTasks;

    private int _mainTasksCompleted = 0;
    private int _additionalTasksCompleted = 0;
    private Dictionary<string, int> _maximumNumberEnemies;
    private Dictionary<string, int> _numberDestroyedEnemies;
    
    private bool _isCompleteLevel = false;

    static public ControllerTasks instance;

    public int maxMainTasks = 1;
    public int maxAdditionalTasks = 3;

    public int MainTasksCompleted
    {
        get
        {
            return _mainTasksCompleted;
        }
        set
        {
            if (value > 0)
            {
                _mainTasksCompleted = value;

                if (_mainTasksCompleted >= maxMainTasks)
                {
                    if (Purpose.purposeCompleteLevel != null)
                    {
                        Purpose.purposeCompleteLevel.ActivatingGoal();
                    }
                    else
                    {
                        Debug.LogError("No object assigned to it for the " +
                            "purpose of completing the mission");
                    }
                }

                SetTextTasks();
            }
        }
    }

    public int AdditionalTasksCompleted
    {
        get
        {
            return _additionalTasksCompleted;
        }
        set
        {
            if (value > 0)
            {
                _additionalTasksCompleted = value;
                SetTextTasks();
            }
        }
    }

    public void CelebrateDestructionEnemy(string keyName)
    {
        if (_numberDestroyedEnemies.ContainsKey(keyName))
        {
            _numberDestroyedEnemies[keyName]++;
        }
        else
        {
            _numberDestroyedEnemies.Add(keyName, 1);
        }
        SetTextTasks();
    }

    public void CompleteLevel()
    {
        _isCompleteLevel = true;
        SetTextTasks();
        Debug.Log("Level Complite");
    }

    public void Init()
    {
        instance = this;

        _maximumNumberEnemies = Manager.loadScene.maximumNumberEnemies;
        _numberDestroyedEnemies = new Dictionary<string, int>();

        SetTextTasks();
    }

    private void SetTextTasks()
    {
        _windowTasks.task1.text = LocalizationManager.Localize(
            "WindowTasks.Tasks1", _mainTasksCompleted, maxMainTasks);
        _windowTasks.task2.text = LocalizationManager.Localize(
            "WindowTasks.Tasks2");
        _windowTasks.task3.text = LocalizationManager.Localize(
            "WindowTasks.Tasks3", _additionalTasksCompleted, 
            maxAdditionalTasks);
        
        string textTask4 = LocalizationManager.Localize("WindowTasks.Tasks4");
        Dictionary<string, bool> completedTask4 = 
            new Dictionary<string, bool>();
        foreach (var itemNumberEnemie in _maximumNumberEnemies)
        {
            string keyName = itemNumberEnemie.Key;
            int maximumNumberEnemies = itemNumberEnemie.Value;
            int numberDestroyedEnemies = 0;
            if (_numberDestroyedEnemies.ContainsKey(keyName))
            {
                numberDestroyedEnemies = _numberDestroyedEnemies[keyName];
            }

            if (numberDestroyedEnemies >= maximumNumberEnemies)
            {
                completedTask4[keyName] = true;
            }
            else
            {
                completedTask4[keyName] = false;
            }

            string nameEnemy = LocalizationManager.Localize(keyName + "_Whom");

            textTask4 += "\n" + LocalizationManager.Localize(
                "WindowTasks.TaskDescription4", nameEnemy, 
                numberDestroyedEnemies, maximumNumberEnemies);
        }
        _windowTasks.task4.text = textTask4;

        Color colorTasksCompleted = _windowTasks.colorTasksCompleted;
        if (_mainTasksCompleted >= maxMainTasks)
        {
            _windowTasks.task1.color = colorTasksCompleted;
        }
        if (_additionalTasksCompleted >= maxAdditionalTasks)
        {
            _windowTasks.task3.color = colorTasksCompleted;
        }
        bool isFullCompletedTask4 = true;
        foreach (bool isItemCompeteTask in completedTask4.Values)
        {
            if (!isItemCompeteTask)
            {
                isFullCompletedTask4 = false;
            }
        }
        if (isFullCompletedTask4)
        {
            _windowTasks.task4.color = colorTasksCompleted;
        }
        if (_isCompleteLevel)
        {
            _windowTasks.task2.color = colorTasksCompleted;
        }
    }
}
