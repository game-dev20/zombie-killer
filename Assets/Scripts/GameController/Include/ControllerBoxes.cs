﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerBoxes : MonoBehaviour
{
    [SerializeField] private Box[] _prefabBoxes;
    [SerializeField] private int[] _numberBoxes;
    [SerializeField] private CharacterController _player;

    public float minRadiusBoxAppearance = 5.0f;
    public float maxRadiusBoxAppearance = 15.0f;
    public float maximumВistanceBox = 20.0f;
    public float timeBoxRebirth = 30.0f;

    private float _objectCreationBoundariesMinX;
    private float _objectCreationBoundariesMaxX;
    private float _objectCreationBoundariesMinZ;
    private float _objectCreationBoundariesMaxZ;
    private float _cellWidth;
    private float _cellHeight;
    private float _offsetX;
    private float _offsetZ;
    private int _widthMap;
    private int _heightMap;
    private Transform _transformPlayer;
    private Box[] _boxes;

    public void Init()
    {
        _widthMap = Manager.loadScene.widthMap;
        _heightMap = Manager.loadScene.heightMap;
        _cellWidth = Manager.loadScene.cellWidth;
        _cellHeight = Manager.loadScene.cellHeight;
        _offsetX = _cellWidth / 2f;
        _offsetZ = _cellHeight / 2f;

        _objectCreationBoundariesMinX = _offsetX;
        _objectCreationBoundariesMaxX = _offsetX + (_widthMap - 1) * _cellWidth;
        _objectCreationBoundariesMinZ = _offsetZ;
        _objectCreationBoundariesMaxZ = _offsetZ + (_heightMap - 1) * 
            _cellHeight;

        _transformPlayer = _player.transform;

        CreateBoxes();
        StartCoroutine(AppearanceBoxes());
        StartCoroutine(RemoveBoxes());
    }

    private void CreateBoxes()
    {
        int countBoxes = 0;
        foreach (int count in _numberBoxes)
        {
            countBoxes += count;
        }
        _boxes = new Box[countBoxes];

        int countTypeBoxes = _numberBoxes.Length;
        int indexBox = 0;
        for (int indexType = 0; indexType < countTypeBoxes; indexType++)
        {
            int countBoxesCurrentType = _numberBoxes[indexType];
            for (int i = indexBox; i < (countBoxesCurrentType + indexBox); i++)
            {
                _boxes[i] = Instantiate(_prefabBoxes[indexType], Vector3.zero, 
                    Quaternion.identity);
                _boxes[i].Initialize();
                _boxes[i].indexType = indexType;
                _boxes[i].gameObjectBox.SetActive(false);
            }
            indexBox += countBoxesCurrentType;
        }
    }

    private IEnumerator AppearanceBoxes()
    {
        while (true)
        {
            foreach(Box box in _boxes)
            {
                if(!box.gameObjectBox.activeSelf)
                {
                    Vector3 newPositionBox = GetRandomPosition();
                    box.transformBox.position = newPositionBox;
                    box.isOpen = false;
                    box.animatorBox.SetBool("IsOpen", false);
                    box.gameObjectBox.SetActive(true);
                }
            }

            yield return new WaitForSeconds(timeBoxRebirth);
        }
    }

    private IEnumerator RemoveBoxes()
    {
        while (true)
        {
            Vector3 positionPlayer = _transformPlayer.position;
            foreach (Box box in _boxes)
            {
                if (!box.gameObjectBox.activeSelf)
                {
                    continue;
                }

                Vector3 positionBox = box.transformBox.position;
                if (
                    Vector3.Distance(positionPlayer, positionBox) > 
                    maximumВistanceBox
                )
                {
                    box.gameObjectBox.SetActive(false);
                }
            }

            yield return new WaitForSeconds(timeBoxRebirth);
        }
    }

    private Vector3 GetRandomPosition()
    {
        Vector3 positionPlayer = _transformPlayer.position;
        Vector3 position = Vector3.zero;
        float minDistance;
        do
        {
            float angle = Random.Range(0f, 360f) * Mathf.PI / 180f;
            float radiusCreate = Random.Range(minRadiusBoxAppearance,
                maxRadiusBoxAppearance);
            float posX = positionPlayer.x + radiusCreate * Mathf.Cos(angle);
            float posZ = positionPlayer.z + radiusCreate * Mathf.Sin(angle);
            posX = Mathf.Round(posX / _cellWidth) * _cellWidth + _offsetX;
            posZ = Mathf.Round(posZ / _cellHeight) * _cellHeight + _offsetZ;
            position = new Vector3(posX, 0f, posZ);

            if (position.x < _objectCreationBoundariesMinX)
            {
                position.x = _objectCreationBoundariesMinX;
            }
            else if (position.x > _objectCreationBoundariesMaxX)
            {
                position.x = _objectCreationBoundariesMaxX;
            }
            if (position.z < _objectCreationBoundariesMinZ)
            {
                position.z = _objectCreationBoundariesMinZ;
            }
            else if (position.z > _objectCreationBoundariesMaxZ)
            {
                position.z = _objectCreationBoundariesMaxZ;
            }

            minDistance = Vector3.Distance(position, positionPlayer);
            foreach(Box itemBox in _boxes)
            {
                Vector3 positionBox = itemBox.transformBox.position;
                float itemDistance = Vector3.Distance(position, positionBox);
                if(itemDistance < minDistance)
                {
                    minDistance = itemDistance;
                }
            }
        }
        while (minDistance < 1f);
        
        return position;
    }
}
