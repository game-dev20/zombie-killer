﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ManagerLoacalization))]
[RequireComponent(typeof(ManagerLoadScene))]
public class Manager : MonoBehaviour
{
    static public ManagerLoacalization loacalization;
    static public ManagerLoadScene loadScene;

    private List<IGameManager> _startSequence = new List<IGameManager>();

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
                
        loacalization = GetComponent<ManagerLoacalization>();
        loadScene = GetComponent<ManagerLoadScene>();

        _startSequence.Add(loacalization);
        _startSequence.Add(loadScene);

        StartCoroutine(StartupManagers());
    }

    private IEnumerator StartupManagers()
    {
        foreach (IGameManager manager in _startSequence)
        {
            manager.Startup();
        }

        yield return null;

        int numModules = _startSequence.Count;
        int numReady = 0;

        while (numReady < numModules)
        {
            int lastReady = numReady;
            numReady = 0;

            foreach (IGameManager manager in _startSequence)
            {
                if (manager.status == ManagerStatus.Started)
                {
                    numReady++;
                }
            }

            if (numReady > lastReady)
            {
                Debug.Log("Progress: " + numReady + "/" + numModules);
            }

            yield return null;
        }

        Debug.Log("All managers started up");

        loadScene.LoadScene();
    }
}
