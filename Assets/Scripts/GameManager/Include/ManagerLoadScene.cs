﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerLoadScene : MonoBehaviour, IGameManager
{
    public ManagerStatus status { get; private set; }

    public Dictionary<string, int> maximumNumberEnemies = 
        new Dictionary<string, int> 
        {
            ["TextEnemies.Beetle"] = 13, 
            ["TextEnemies.NameZombie"] = 26 
        };
    public int seed = 27454;
    public int widthMap = 20;
    public int heightMap = 20;
    public int numberHalls = 10;
    public int numberSmallRooms = 10;
    public int numberAdditionalDoors = 10;
    public int numberCorpses = 40;
    public int numberDecorations = 40;
    public float cellWidth = 4.5f;
    public float cellHeight = 4.5f;

    public void Startup()
    {
        Debug.Log("ManagerLoadScene manager started...");

        status = ManagerStatus.Started;
    }

    public void LoadScene()
    {
        SceneManager.LoadScene("Level");
    }
}
