﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Corpse : MonoBehaviour
{
    public string[] namesPoses;

    private Transform _transform;
    private Animator _animator;

    private void Start()
    {
        _transform = transform;
        _animator = GetComponent<Animator>();

        SetRotation();
        SetPose();
    }

    private void SetRotation()
    {
        float angle = Random.Range(0f, 360f);
        Vector3 rotation = new Vector3(0f, angle, 0f);

        _transform.eulerAngles = rotation;
    }

    private void SetPose()
    {
        int countPose = namesPoses.Length;
        int index = Random.Range(0, countPose);
        string namePose = namesPoses[index];

        _animator.SetBool(namePose, true);
    }
}