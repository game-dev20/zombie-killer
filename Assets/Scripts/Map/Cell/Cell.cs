﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public GameObject wallLeft;
    public GameObject wallBottom;

    public GameObject wallRight;
    public GameObject wallTop;
    public GameObject doorLeft;
    public GameObject doorBottom;
    public GameObject triggerDoorBottom;
    public GameObject objectLight;
    public GameObject externalLightBottom;
    public GameObject externalLightLeft;
    public GameObject innerLeftNonGlowingLamp;

    public GameObject[] prefabsBlood;
    public Transform positionBloodFloor;
    public Transform positionBloodWallLeftInside;
    public Transform positionBloodWallBottomInside;
    public Transform positionBloodWallLeftOutside;
    public Transform positionBloodWallBottomOutside;

    public GameObject[] prefabDecorations;

    public TextMesh text;

    public void InstallationDecorations()
    {
        int countPrefabs = prefabDecorations.Length;

        if (countPrefabs == 0)
        {
            return;
        }

        int index = Random.Range(0, countPrefabs);
        GameObject prefab = prefabDecorations[index];
        Instantiate(prefab, transform.position, Quaternion.identity);
    }

    private void InstallationObjectBlood(Transform parent)
    {
        int countPrefabs = prefabsBlood.Length;
        
        if (countPrefabs == 0)
        {
            return;
        }

        int index = Random.Range(0, countPrefabs);
        GameObject prefab = prefabsBlood[index];
        Vector3 position = parent.position;
        Quaternion rotation = parent.rotation;
        Instantiate(prefab, position, rotation, parent);
    }

    public void InstallationBloodFloor()
    {
        InstallationObjectBlood(positionBloodFloor);
    }

    public void InstallationBloodWallLeftInside()
    {
        InstallationObjectBlood(positionBloodWallLeftInside);
    }

    public void InstallationBloodWallBottomInside()
    {
        InstallationObjectBlood(positionBloodWallBottomInside);
    }

    public void InstallationBloodWallLeftOutside()
    {
        InstallationObjectBlood(positionBloodWallLeftOutside);
    }

    public void InstallationBloodWallBottomOutside()
    {
        InstallationObjectBlood(positionBloodWallBottomOutside);
    }

    public void RemoveWallLeft()
    {
        Destroy(wallLeft);
    }

    public void RemoveWallBottom()
    {
        Destroy(wallBottom);
    }

    public void RemoveWallRight()
    {
        Destroy(wallRight);
    }

    public void RemoveWallTop()
    {
        Destroy(wallTop);
    }

    public void RemoveDoorLeft()
    {
        Destroy(doorLeft);
    }

    public void RemoveDoorBottom()
    {
        Destroy(doorBottom);
    }

    public void RemoveTriggerDoorBottom()
    {
        Destroy(triggerDoorBottom);
    }

    public void RemoveNonGlowingLamp()
    {
        if (wallLeft != null)
        {
            Destroy(innerLeftNonGlowingLamp);
        }
    }

    public void RemoveLight()
    {
        if (wallLeft != null)
        {
            Destroy(objectLight);
        }
    }

    public void RemoveExternalLightBottom()
    {
        if (wallBottom != null)
        {
            Destroy(externalLightBottom);
        }
    }

    public void RemoveExternalLightLeft()
    {
        if (wallLeft != null)
        {
            Destroy(externalLightLeft);
        }
    }
}
