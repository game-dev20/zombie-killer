﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDoor : MonoBehaviour
{
    [SerializeField] private GameObject _door;

    public Vector3 changePosition;

    private Vector3 _closePosition;
    private Vector3 _openPosition;

    private bool _isInitialization = false;

    private void Start()
    {
        _closePosition = _door.transform.position;
        _openPosition = _closePosition + changePosition;

        _isInitialization = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!_isInitialization)
        {
            return;
        }

        iTween.MoveTo(_door, _openPosition, 1f);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!_isInitialization)
        {
            return;
        }

        iTween.MoveTo(_door, _closePosition, 1f);
    }
}
