﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(ReactiveTarget))]
public class CharacteristicsEnemy : MonoBehaviour
{
    public string keyName = "";
    public int maxHealth = 100;
    public int health = 100;
    public int forceBlow = 50;
    public float walkingSpeed = 2.0f;
    public float runningSpeed = 4.0f;
    public float speedRotation = 10.0f;
    public float distanceStrike = 2.0f;
    public float timeBetweenStrokes = 1.0f;

    public EnemyMode enemyMode = EnemyMode.PeaceMind;
    public float recentExcitement = 0f;
    public bool isAlive = true;
    public bool isDecayed = false;
    public bool waitingDecay = false;
    public bool processDecay = false;
    public bool notHit = false;
    public GameObject gameObjectEnemy;
    public Transform transformEnemy;
    public Animator animatorEnemy;
    public CharacterController characterControllerEnemy;
    public Quaternion angleRotation;
    public Vector3 lastPositionGoal;
    public ReactiveTarget reactiveTargetEnemy;

    public void Initialize()
    {
        gameObjectEnemy = gameObject;
        transformEnemy = transform;
        animatorEnemy = GetComponent<Animator>();
        characterControllerEnemy = GetComponent<CharacterController>();
        reactiveTargetEnemy = GetComponent<ReactiveTarget>();
    }
}
