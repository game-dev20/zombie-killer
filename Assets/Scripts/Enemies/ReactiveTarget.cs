﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacteristicsEnemy))]
public class ReactiveTarget : MonoBehaviour
{
    public HealthLevel sliderHealthEnemy = null;
    public Transform transformObject = null;

    private CharacterController _characterController;
    private Animator _animator;
    private CharacteristicsEnemy _characteristics;

    private void Start()
    {
        transformObject = transform;

        _characterController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _characteristics = GetComponent<CharacteristicsEnemy>();
    }

    public void ReactToHit(int value)
    {
        int health = _characteristics.health;
        int maxHealth = _characteristics.maxHealth;

        _characteristics.enemyMode = EnemyMode.Attack;
        _characteristics.recentExcitement = Time.realtimeSinceStartup;
        health -= value;
        if(health < 0)
        {
            health = 0;
        }

        if(sliderHealthEnemy != null)
        {
            sliderHealthEnemy.SetHealth(health, maxHealth);
        }

        _characteristics.health = health;

        if (health == 0)
        {
            _characteristics.isAlive = false;
            _characterController.enabled = false;
            _animator.SetBool("IsDead", true);
            string keyName = _characteristics.keyName;
            ControllerTasks controllerTasks = ControllerTasks.instance;
            if (controllerTasks != null)
            {
                controllerTasks.CelebrateDestructionEnemy(keyName);
            }
        }
    }
}
