﻿public enum DirectionGoal 
{
    Up,
    Down,
    Left,
    Right
}
