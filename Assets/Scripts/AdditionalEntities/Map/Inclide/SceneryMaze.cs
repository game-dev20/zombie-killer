﻿using UnityEngine;

public class SceneryMaze
{
    public void PlacementCorpses(ref MazeGeneratoeCell[,] maze, 
        int numberCorpses)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);
        for (int i = 0; i < numberCorpses; i++)
        {
            int x;
            int y;
            do
            {
                x = Random.Range(0, width);
                y = Random.Range(0, height);
            }
            while (
                (x == 0 && y == 0) ||
                maze[x, y].isMainGoal ||
                maze[x, y].isAdditionalGoal ||
                maze[x, y].isGoalCompletedLevel
            );

            maze[x, y].isCorpse = true;
        }
    }

    public void SettingScenery(ref MazeGeneratoeCell[,] maze,
        int numberDecorations)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);
        for (int i = 0; i < numberDecorations; i++)
        {
            int x;
            int y;
            do
            {
                x = Random.Range(0, width);
                y = Random.Range(0, height);
            }
            while (
                (x == 0 && y == 0) ||
                maze[x, y].isMainGoal ||
                maze[x, y].isAdditionalGoal ||
                maze[x, y].isGoalCompletedLevel ||
                maze[x, y].isCorpse
            );

            maze[x, y].isScenery = true;
        }
    }

    public void PlacementBlood(ref MazeGeneratoeCell[,] maze)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);
        float probability = 0.1f;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                MazeGeneratoeCell cell = maze[x, y];
                if (
                    cell.isCorpse ||
                    (Random.Range(0f, 1f) <= probability)
                )
                {
                    cell.isBloodFloor = true;
                }

                if (
                    (Random.Range(0f, 1f) <= probability) &&
                    cell.isWallLeft
                )
                {
                    cell.isBloodWallLeftInside = true;
                }
                if (
                    (Random.Range(0f, 1f) <= probability) &&
                    cell.isWallBottom
                )
                {
                    cell.isBloodWallBottomInside = true;
                }
                if (
                    (Random.Range(0f, 1f) <= probability) &&
                    cell.isWallLeft &&
                    (x != 0)
                )
                {
                    cell.isBloodWallLeftOutside = true;
                }
                if (
                    (Random.Range(0f, 1f) <= probability) &&
                    cell.isWallBottom &&
                    (y != 0)
                )
                {
                    cell.isBloodWallBottomOutside = true;
                }
            }
        }
    }
}
