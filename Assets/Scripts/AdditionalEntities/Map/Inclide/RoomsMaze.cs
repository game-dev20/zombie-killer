﻿public class RoomsMaze : BasicMazeGenerator
{
    public void RemovalNeighboringWalls(ref MazeGeneratoeCell[,] maze,
        int numberSmallRooms)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        for (int i = 0; i < numberSmallRooms; i++)
        {
            int x = UnityEngine.Random.Range(1, width - 1);
            int y = UnityEngine.Random.Range(1, height - 1);

            MazeGeneratoeCell cellCurrent = maze[x, y];
            MazeGeneratoeCell cellBottom = maze[x, y - 1];
            MazeGeneratoeCell cellLeft = maze[x - 1, y];
            MazeGeneratoeCell cellRight = maze[x + 1, y];
            MazeGeneratoeCell cellTop = maze[x, y + 1];

            RemoveWalls(cellCurrent, cellBottom);
            RemoveWalls(cellCurrent, cellLeft);
            RemoveWalls(cellCurrent, cellRight);
            RemoveWalls(cellCurrent, cellTop);
        }
    }

    public void RemovingBlockWalls(ref MazeGeneratoeCell[,] maze, 
        int numberHalls)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);
        for (int i = 0; i < numberHalls; i++)
        {
            int x = UnityEngine.Random.Range(1, width - 2);
            int y = UnityEngine.Random.Range(1, height - 2);

            MazeGeneratoeCell cellLeftBottom = maze[x, y];
            MazeGeneratoeCell cellRightBottom = maze[x + 1, y];
            MazeGeneratoeCell cellLeftTop = maze[x, y + 1];
            MazeGeneratoeCell cellRightTop = maze[x + 1, y + 1];

            RemoveWalls(cellLeftBottom, cellRightBottom);
            RemoveWalls(cellLeftBottom, cellLeftTop);
            RemoveWalls(cellLeftTop, cellRightTop);
            RemoveWalls(cellRightBottom, cellRightTop);

            MazeGeneratoeCell cellLeftBottomBottom = maze[x, y - 1];
            MazeGeneratoeCell cellRightBottomBottom = maze[x + 1, y - 1];
            MazeGeneratoeCell cellLeftLeftTop = maze[x - 1, y + 1];
            MazeGeneratoeCell cellLeftLeftBottom = maze[x - 1, y];
            MazeGeneratoeCell cellLeftTopTop = maze[x, y + 2];
            MazeGeneratoeCell cellRightTopTop = maze[x + 1, y + 2];
            MazeGeneratoeCell cellRightRightTop = maze[x + 2, y + 1];
            MazeGeneratoeCell cellRightRightBottom = maze[x + 2, y];

            InstallationDoors(cellLeftBottom, cellLeftBottomBottom);
            InstallationDoors(cellLeftBottom, cellLeftLeftBottom);
            InstallationDoors(cellRightBottom, cellRightBottomBottom);
            InstallationDoors(cellRightBottom, cellRightRightBottom);
            InstallationDoors(cellLeftTop, cellLeftLeftTop);
            InstallationDoors(cellLeftTop, cellLeftTopTop);
            InstallationDoors(cellRightTop, cellRightTopTop);
            InstallationDoors(cellRightTop, cellRightRightTop);
        }
    }

    public void AlignmentDoors(ref MazeGeneratoeCell[,] maze,
        int numberAdditionalDoors)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        for (int i = 0; i < numberAdditionalDoors; i++)
        {
            int x = UnityEngine.Random.Range(1, width - 1);
            int y = UnityEngine.Random.Range(1, height - 1);

            MazeGeneratoeCell cellCurrent = maze[x, y];
            MazeGeneratoeCell cellRight = maze[x + 1, y];
            MazeGeneratoeCell cellTop = maze[x, y + 1];

            if (
                !cellCurrent.isWallBottom &&
                !cellTop.isWallBottom &&
                cellCurrent.isWallLeft &&
                cellRight.isWallLeft
            )
            {
                cellCurrent.isDoorBottom = true;
            }
            else if (
                !cellCurrent.isWallLeft &&
                !cellRight.isWallLeft &&
                cellCurrent.isWallBottom &&
                cellTop.isWallBottom
            )
            {
                cellCurrent.isDoorLeft = true;
            }
        }
    }

    private void InstallationDoors(MazeGeneratoeCell chosen,
        MazeGeneratoeCell current)
    {
        if (chosen.x < current.x && !current.isWallLeft)
        {
            current.isDoorLeft = true;
        }
        else if (chosen.x > current.x && !chosen.isWallLeft)
        {
            chosen.isDoorLeft = true;
        }
        else if (chosen.y < current.y && !current.isWallBottom)
        {
            current.isDoorBottom = true;
        }
        else if (chosen.y > current.y && !chosen.isWallBottom)
        {
            chosen.isDoorBottom = true;
        }
    }
}
