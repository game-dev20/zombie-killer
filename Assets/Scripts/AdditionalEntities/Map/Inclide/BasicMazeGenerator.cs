﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMazeGenerator
{
    protected int _seed;
    protected int _width;
    protected int _height;

    public BasicMazeGenerator(int width = 10, int height = 10, int seed = 12345)
    {
        _seed = seed;
        _width = width;
        _height = height;
    }

    public virtual MazeGeneratoeCell[,] GenerateMaze()
    {
        UnityEngine.Random.InitState(_seed);
        MazeGeneratoeCell[,] maze = new MazeGeneratoeCell[_width, _height];

        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                maze[x, y] = new MazeGeneratoeCell();
                maze[x, y].x = x;
                maze[x, y].y = y;
            }
        }

        RemoveWalls(maze);

        return maze;
    }

    protected void RemoveWalls(MazeGeneratoeCell chosen,
        MazeGeneratoeCell current)
    {
        if (chosen.x < current.x)
        {
            current.isWallLeft = false;
            current.isDoorLeft = false;
            current.isLight = false;
        }
        else if (chosen.x > current.x)
        {
            chosen.isWallLeft = false;
            chosen.isDoorLeft = false;
            current.isLight = false;
        }
        else if (chosen.y < current.y)
        {
            current.isWallBottom = false;
            current.isDoorBottom = false;
        }
        else if (chosen.y > current.y)
        {
            chosen.isWallBottom = false;
            chosen.isDoorBottom = false;
        }
    }

    protected List<MazeGeneratoeCell> GetNeighborsCurrentCellIncludingWalls(
        MazeGeneratoeCell[,] maze, List<MazeGeneratoeCell> current,
        ref List<MazeGeneratoeCell> destinationCell)
    {
        List<MazeGeneratoeCell> neighbors = new List<MazeGeneratoeCell>();

        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        foreach (MazeGeneratoeCell itemCell in current)
        {
            int x = itemCell.x;
            int y = itemCell.y;

            bool isAddition = false;

            if (x > 0)
            {
                if (!maze[x - 1, y].isVisited && !itemCell.isWallLeft)
                {
                    neighbors.Add(maze[x - 1, y]);
                    isAddition = true;
                }
            }
            if (x < width - 1)
            {
                if (!maze[x + 1, y].isVisited && !maze[x + 1, y].isWallLeft)
                {
                    neighbors.Add(maze[x + 1, y]);
                    isAddition = true;
                }
            }
            if (y > 0)
            {
                if (!maze[x, y - 1].isVisited && !itemCell.isWallBottom)
                {
                    neighbors.Add(maze[x, y - 1]);
                    isAddition = true;
                }
            }
            if (y < height - 1)
            {
                if (!maze[x, y + 1].isVisited && !maze[x, y + 1].isWallBottom)
                {
                    neighbors.Add(maze[x, y + 1]);
                    isAddition = true;
                }
            }

            if (!isAddition && destinationCell.IndexOf(itemCell) == -1)
            {
                destinationCell.Add(itemCell);
            }
        }

        return neighbors;
    }

    protected void StandartInitialRandomaizer()
    {
        DateTime dateUnix = new DateTime(1970, 1, 1, 0, 0, 0);
        DateTime dateNow = DateTime.Now;
        int newSeed = (int)dateNow.Subtract(dateUnix).TotalSeconds;
        UnityEngine.Random.InitState(newSeed);
    }

    protected void ResetVisitedCells(MazeGeneratoeCell[,] maze)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                maze[x, y].isVisited = false;
            }
        }
    }

    private void RemoveWalls(MazeGeneratoeCell[,] maze)
    {
        MazeGeneratoeCell current = maze[0, 0];
        current.isVisited = true;

        Stack<MazeGeneratoeCell> stack = new Stack<MazeGeneratoeCell>();

        int i = 0;
        do
        {
            List<MazeGeneratoeCell> neighborsCurrentCell =
                GetNeighborsCurrentCell(maze, current);
            int countNeighbors = neighborsCurrentCell.Count;
            if (countNeighbors > 0)
            {
                int index = UnityEngine.Random.Range(0, countNeighbors);
                MazeGeneratoeCell chosen = neighborsCurrentCell[index];

                RemoveWalls(chosen, current);

                chosen.isVisited = true;
                stack.Push(chosen);
                current = chosen;

                if (i % 6 == 2)
                {
                    current.isLight = true;
                }
                i++;
            }
            else
            {
                current = stack.Pop();
            }
        }
        while (stack.Count > 0);
    }

    private List<MazeGeneratoeCell> GetNeighborsCurrentCell(
        MazeGeneratoeCell[,] maze, MazeGeneratoeCell current)
    {
        List<MazeGeneratoeCell> neighbors = new List<MazeGeneratoeCell>();
        int x = current.x;
        int y = current.y;
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        if (x > 0)
        {
            if (!maze[x - 1, y].isVisited)
            {
                neighbors.Add(maze[x - 1, y]);
            }
        }
        if (x < width - 1)
        {
            if (!maze[x + 1, y].isVisited)
            {
                neighbors.Add(maze[x + 1, y]);
            }
        }
        if (y > 0)
        {
            if (!maze[x, y - 1].isVisited)
            {
                neighbors.Add(maze[x, y - 1]);
            }
        }
        if (y < height - 1)
        {
            if (!maze[x, y + 1].isVisited)
            {
                neighbors.Add(maze[x, y + 1]);
            }
        }

        return neighbors;
    }
}
