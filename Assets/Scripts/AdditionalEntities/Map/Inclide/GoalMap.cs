﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalMap : BasicMazeGenerator
{
    public void SettingGoalMap(ref MazeGeneratoeCell[,] maze)
    {
        int width = maze.GetLength(0);
        int height = maze.GetLength(1);

        ResetVisitedCells(maze);

        List<MazeGeneratoeCell> destinationCell = new List<MazeGeneratoeCell>();

        SettingDistanceFromStart(maze, 0, 0, ref destinationCell);

        MazeGeneratoeCell mainGoalCell = destinationCell[0];
        int cellDepth = mainGoalCell.distanceStart;
        foreach (MazeGeneratoeCell itemCell in destinationCell)
        {
            int distance = itemCell.distanceStart;
            if (distance > cellDepth)
            {
                mainGoalCell = itemCell;
                cellDepth = distance;
            }
        }
        mainGoalCell.isMainGoal = true;

        float numberCellsPerTarget = (width - 2) * (height - 2) / 4;
        int minDistanceBetweenTargets =
            (int) Mathf.Sqrt(numberCellsPerTarget) / 2;
        List<MazeGeneratoeCell> listGoals = new List<MazeGeneratoeCell>();
        listGoals.Add(mainGoalCell);
        for (int i = 0; i < 3; i++)
        {
            int xAdditionalGoal;
            int yAdditionalGoal;
            int distance;
            do
            {
                xAdditionalGoal = UnityEngine.Random.Range(1, width - 1);
                yAdditionalGoal = UnityEngine.Random.Range(1, height - 1);
                Vector2 positionSelectCell =
                    new Vector2(xAdditionalGoal, yAdditionalGoal);

                distance = -1;
                foreach (MazeGeneratoeCell itemGoal in listGoals)
                {
                    int xItemGoal = itemGoal.x;
                    int yItemGoal = itemGoal.y;
                    Vector2 positionitemGoal =
                        new Vector2(xItemGoal, yItemGoal);

                    int itemDistance = (int)Vector2.Distance(
                        positionSelectCell, positionitemGoal);
                    if (distance > itemDistance || distance < 0)
                    {
                        distance = itemDistance;
                    }
                }
            }
            while (distance < minDistanceBetweenTargets);
            MazeGeneratoeCell selectGoal =
                maze[xAdditionalGoal, yAdditionalGoal];
            selectGoal.isAdditionalGoal = true;
            listGoals.Add(selectGoal);
        }

        maze[0, 0].isGoalCompletedLevel = true;
    }

    private void SettingDistanceFromStart(MazeGeneratoeCell[,] maze, int startX,
        int startY, ref List<MazeGeneratoeCell> destinationCell)
    {
        int distanceStart = 0;
        List<MazeGeneratoeCell> current = new List<MazeGeneratoeCell>();
        maze[0, 0].isVisited = true;
        maze[0, 0].distanceStart = distanceStart;
        current.Add(maze[0, 0]);

        do
        {
            distanceStart++;
            List<MazeGeneratoeCell> neighborsCurrentCell =
                GetNeighborsCurrentCellIncludingWalls(maze, current,
                ref destinationCell);

            foreach (MazeGeneratoeCell itemCell in neighborsCurrentCell)
            {
                itemCell.isVisited = true;
                itemCell.distanceStart = distanceStart;
            }

            current = neighborsCurrentCell;
        }
        while (current.Count > 0);
    }
}
