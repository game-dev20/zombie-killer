﻿using System.Collections;
using System.Collections.Generic;

public class MazeGeneratoeCell
{
    public int x;
    public int y;

    public bool isWallLeft = true;
    public bool isWallBottom = true;

    public bool isWallRight = false;
    public bool isWallTop = false;
    public bool isDoorLeft = false;
    public bool isDoorBottom = false;

    public bool isVisited = false;

    public bool isMainGoal = false;
    public bool isAdditionalGoal = false;
    public bool isGoalCompletedLevel = false;

    public bool isCorpse = false;

    public bool isBloodFloor = false;
    public bool isBloodWallLeftInside = false;
    public bool isBloodWallBottomInside = false;
    public bool isBloodWallLeftOutside = false;
    public bool isBloodWallBottomOutside = false;

    public bool isScenery = false;

    public bool isLight = false;
    
    public int distanceStart = 0;

    public List<MazeGeneratoeCell> cellsLeadingCurrentMainPurpose =
        new List<MazeGeneratoeCell>();
    public List<MazeGeneratoeCell> cellsLeadingCurrentCompleteLevel = 
        new List<MazeGeneratoeCell>();
    public DirectionGoal directionMainPurpose;
    public DirectionGoal directionCompleteLevel;
}