﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : BasicMazeGenerator
{
    private int _numberHalls;
    private int _numberSmallRooms;
    private int _numberAdditionalDoors;
    private int _numberCorpses;
    private int _numberDecorations;
    
    public MazeGenerator(int width = 10, int height = 10, int numberHalls = 1, 
        int numberSmallRooms = 1, int numberAdditionalDoors = 1, 
        int numberCorpses = 0, int numberDecorations = 0, int seed = 12345) : 
        base(width, height, seed)
    {
        _numberHalls = numberHalls;
        _numberSmallRooms = numberSmallRooms;
        _numberAdditionalDoors = numberAdditionalDoors;
        _numberCorpses = numberCorpses;
        _numberDecorations = numberDecorations;
    }

    public override MazeGeneratoeCell[,] GenerateMaze()
    {
        MazeGeneratoeCell[,] maze = base.GenerateMaze();

        RoomsMaze roomsMaze = new RoomsMaze();
        roomsMaze.RemovalNeighboringWalls(ref maze, _numberSmallRooms);
        roomsMaze.RemovingBlockWalls(ref maze, _numberHalls);
        roomsMaze.AlignmentDoors(ref maze, _numberAdditionalDoors);

        maze[0, 0].isWallBottom = false;
        maze[0, 0].isDoorBottom = true;

        GoalMap goalMap = new GoalMap();
        goalMap.SettingGoalMap(ref maze);

        SceneryMaze sceneryMaze = new SceneryMaze();
        sceneryMaze.PlacementCorpses(ref maze, _numberCorpses);
        sceneryMaze.SettingScenery(ref maze, _numberDecorations);
        sceneryMaze.PlacementBlood(ref maze);
        
        StandartInitialRandomaizer();

        return maze;
    }
}
